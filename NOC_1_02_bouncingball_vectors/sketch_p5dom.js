// The Nature of Code
// Daniel Shiffman
// Modified by Chiyang Lin

// Example 1-1: Bouncing Ball, no vectors
//let x = 100;
//let y = 100;
//let xspeed = 2.5;
//let yspeed = 2;
var x, y ,xspeed, yspeed;
var position;
var velocity;
let input, button;
let xSlider, ySlider, xspeedSlider,yspeedSliders;
function setup() {
  createCanvas(800, 600);
  textSize(15);
  noStroke();

  position = createVector(100, 100);
  velocity = createVector(2.5, 5);

  // create sliders
  xSlider = createSlider(0, 800, 100);
  xSlider.position(20, 20);
  ySlider = createSlider(0, 600, 100);
  ySlider.position(20, 50);
  xspeedSlider = createSlider(0, 10, 2.5);
  xspeedSlider.position(20, 80);
  yspeedSlider = createSlider(0, 10, 2);
  yspeedSlider.position(20, 110);

  button = createButton('apply');
  button.position(20 , 140);
  button.mousePressed(apply);
  console.log("x = ",position.x);

}

function apply() {
  position.x = xSlider.value();
  position.y = ySlider.value();
  velocity.x = xspeedSlider.value();
  velocity.y = yspeedSlider.value();
}

function draw() {
  background(51);
  text('x:', xSlider.x * 2 + xSlider.width, 35);
  text('y:', ySlider.x * 2 + ySlider.width, 65);
  text('x-speed', xspeedSlider.x * 2 + xspeedSlider.width, 95);
  text('y-speed', yspeedSlider.x * 2 + yspeedSlider.width, 125);
  // Add the current speed to the position.
  //x = x + xspeed;
  //y = y + yspeed;

  // Add the current speed to the position.
  position.add(velocity);

  //if ((x > width) || (x < 0)) {
  //  xspeed = xspeed * -1;
  //}
  //if ((y > height) || (y < 0)) {
  //  yspeed = yspeed * -1;
  //}

  if ((position.x > width) || (position.x < 0)) {
    velocity.x = velocity.x * -1;
  }
  if ((position.y > height) || (position.y < 0)) {
    velocity.y = velocity.y * -1;
  }
  // Display circle at x position
  stroke(0);
  strokeWeight(2);
  fill(127);
  ellipse(position.x, position.y, 48, 48);
}
