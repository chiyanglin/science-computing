// The Nature of Code
// Daniel Shiffman
// Modified by Chiyang Lin

// Example 1-1: Bouncing Ball, no vectors
//let x = 100;
//let y = 100;
//let xspeed = 2.5;
//let yspeed = 2;

//var x = 0;
//var y = 0;
//var xspeed = 0;
//var yspeed = 0;
var position;
var velocity;

var number = 0;
var color = 51;
var colorstep = 0.01;

var cnv;

function centerCanvas() {
  var x = (windowWidth - width) / 2;
  var y = (windowHeight - height) / 2;
  cnv.position(x, y);
}

//function setup() {
//  cnv = createCanvas(100, 100);
//  centerCanvas();
//  background(255, 0, 200);
//}

function windowResized() {
  centerCanvas();
}



function setup() {
  //createCanvas(640, 360);
  cnv = createCanvas(640, 360);
  centerCanvas();
  background(255, 0, 200);
  position = createVector(100, 100);
  velocity = createVector(2.5, 5);
  console.log("x = ",position.x);
  console.log("y = ",position.y);
  console.log("xspeed = ",velocity.x);
  console.log("yspeed = ",velocity.y);
}

function apply() {
  position.x = parseFloat(document.getElementById("coord_x").value);
  position.y = parseFloat(document.getElementById("coord_y").value);
  velocity.x = parseFloat(document.getElementById("coord_speedx").value);
  velocity.y = parseFloat(document.getElementById("coord_speedy").value);
  //number = document.getElementById("myNumber").value;
  //x = 100;
  //y = 100;
  //xspeed = 2.5;
  //yspeed = 2;
  console.log("position.x = ",position.x);
}

function draw() {
  //color += colorstep;
  background(51);
  //console.log("x = ",x);
  // Add the current speed to the position.
  //x = x + xspeed;
  //y = y + yspeed;
  position.add(velocity);
  //console.log("x = ",x);
  //if ((x > width) || (x < 0)) {
  //  xspeed = xspeed * -1;
  //}
  //if ((y > height) || (y < 0)) {
  //  yspeed = yspeed * -1;
  //}

  if ((position.x > width) || (position.x < 0)) {
    velocity.x = velocity.x * -1;
  }
  if ((position.y > height) || (position.y < 0)) {
    velocity.y = velocity.y * -1;
  }
  // Display circle at x position
  stroke(0);
  strokeWeight(2);
  fill(127);
  ellipse(position.x, position.y, 48, 48);
  document.getElementById("position_x").innerHTML = position.x;
  document.getElementById("position_y").innerHTML = position.y;
  document.getElementById("velocity_x").innerHTML = velocity.x;
  document.getElementById("velocity_y").innerHTML = velocity.y;
}
