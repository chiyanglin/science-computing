// The Nature of Code
// Daniel Shiffman
// Modified by Chiyang Lin


let b;
let input, button;
let xSlider, ySlider, xspeedSlider,yspeedSliders;
function setup() {
  createCanvas(800, 600);
  textSize(15);
  noStroke();

  b = new Ball();

  // create sliders
  xSlider = createSlider(0, 800, 100);
  xSlider.position(20, 20);
  ySlider = createSlider(0, 600, 100);
  ySlider.position(20, 50);
  xspeedSlider = createSlider(0, 10, 2.5);
  xspeedSlider.position(20, 80);
  yspeedSlider = createSlider(0, 10, 2);
  yspeedSlider.position(20, 110);

  button = createButton('apply');
  button.position(20 , 140);
  button.mousePressed(apply);
  console.log("x = ",b.position.x);

}

function apply() {
  b.position.x = xSlider.value();
  b.position.y = ySlider.value();
  b.velocity.x = xspeedSlider.value();
  b.velocity.y = yspeedSlider.value();
}

function draw() {
  background(51);
  text('x:', xSlider.x * 2 + xSlider.width, 35);
  text('y:', ySlider.x * 2 + ySlider.width, 65);
  text('x-speed', xspeedSlider.x * 2 + xspeedSlider.width, 95);
  text('y-speed', yspeedSlider.x * 2 + yspeedSlider.width, 125);


  b.update();
  b.display();

}
