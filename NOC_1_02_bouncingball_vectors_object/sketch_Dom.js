// The Nature of Code
// Daniel Shiffman
// Modified by Chiyang Lin

// Example 1-2: Bouncing Ball, with p5.Vector!

var number = 0;
var color = 51;
var colorstep = 0.01;

var cnv;

function centerCanvas() {
  var x = (windowWidth - width) / 2;
  var y = (windowHeight - height) / 2;
  cnv.position(x, y);
}

function windowResized() {
  centerCanvas();
}

function setup() {
  //createCanvas(640, 360);
  cnv = createCanvas(640, 360);
  centerCanvas();
  background(255, 0, 200);
  b = new Ball();
  console.log("x = ",b.position.x);
  console.log("y = ",b.position.y);
  console.log("xspeed = ",b.velocity.x);
  console.log("yspeed = ",b.velocity.y);
}

function apply() {
  b.position.x = parseFloat(document.getElementById("coord_x").value);
  b.position.y = parseFloat(document.getElementById("coord_y").value);
  b.velocity.x = parseFloat(document.getElementById("coord_speedx").value);
  b.velocity.y = parseFloat(document.getElementById("coord_speedy").value);

  console.log("position.x = ",b.position.x);
}

function draw() {

  background(51);
  b.update();
  b.display();

  document.getElementById("position_x").innerHTML = b.position.x;
  document.getElementById("position_y").innerHTML = b.position.y;
  document.getElementById("velocity_x").innerHTML = b.velocity.x;
  document.getElementById("velocity_y").innerHTML = b.velocity.y;
}
