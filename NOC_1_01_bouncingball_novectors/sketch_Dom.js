// The Nature of Code
// Daniel Shiffman
// Modified by Chiyang Lin

// Example 1-1: Bouncing Ball, no vectors
//let x = 100;
//let y = 100;
//let xspeed = 2.5;
//let yspeed = 2;

var x = 0;
var y = 0;
var xspeed = 0;
var yspeed = 0;
var number = 0;
var color = 51;
var colorstep = 0.01;

var cnv;

function centerCanvas() {
  var x = (windowWidth - width) / 2;
  var y = (windowHeight - height) / 2;
  cnv.position(x, y);
}

//function setup() {
//  cnv = createCanvas(100, 100);
//  centerCanvas();
//  background(255, 0, 200);
//}

function windowResized() {
  centerCanvas();
}



function setup() {
  //createCanvas(640, 360);
  cnv = createCanvas(640, 360);
  centerCanvas();
  background(255, 0, 200);

  console.log("x = ",x);
  console.log("y = ",y);
  console.log("xspeed = ",xspeed);
  console.log("yspeed = ",yspeed);
}

function apply() {
  x = parseFloat(document.getElementById("coord_x").value);
  y = parseFloat(document.getElementById("coord_y").value);
  xspeed = parseFloat(document.getElementById("coord_speedx").value);
  yspeed = parseFloat(document.getElementById("coord_speedy").value);
  //number = document.getElementById("myNumber").value;
  //x = 100;
  //y = 100;
  //xspeed = 2.5;
  //yspeed = 2;
  console.log("x = ",x);
}

function draw() {
  //color += colorstep;
  background(51);
  //console.log("x = ",x);
  // Add the current speed to the position.
  x = x + xspeed;
  y = y + yspeed;
  //console.log("x = ",x);
  if ((x > width) || (x < 0)) {
    xspeed = xspeed * -1;
  }
  if ((y > height) || (y < 0)) {
    yspeed = yspeed * -1;
  }

  // Display circle at x position
  stroke(0);
  strokeWeight(2);
  fill(127);
  ellipse(x, y, 48, 48);
}
