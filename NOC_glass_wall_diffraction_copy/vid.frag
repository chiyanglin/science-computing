precision mediump float;

// lets grab texcoords just for fun
varying vec2 vTexCoord;

// our texture coming from p5
uniform sampler2D tex0;
uniform float time;
uniform vec2 uMousePos;

float drawCircle( vec2 st, vec2 pos, float size ){
    
    float result = distance( st, pos);
    result = smoothstep( result, result + 0.1 * (sin(time* 2.) * 0.5 + 0.5), 0.15);
    
    return result;
}

vec2 createGrid( in vec2 st, in vec2 grid, out vec2 indices) {
    
    st *= grid;
    
    indices = floor(st);
    st = fract(st);

    return st;
}

void main() {

  vec2 uv = vTexCoord;
  // the texture is loaded upside down and backwards by default so lets flip it
    uv = 1.0 - uv;
    vec2 st = uv;
    vec2 test = uv;
    vec2 indices;
    st = createGrid(st, vec2(100.), indices);


  // add the distortion to our texture coordinates
  vec4 tex = texture2D(tex0, uv + st / 9.);
  float c = drawCircle(test, uMousePos, 0.05);

      vec2 toCenter = vec2(0.5)-st;
    float angle = atan(toCenter.y,toCenter.x);
    float radius = length(toCenter)*2.0;
  float z = radius * 10.*sin(time/1.+ atan(st.y, st.x));
  
  vec4 originalTex = texture2D(tex0, test);
  tex = mix(tex, originalTex, c);

  gl_FragColor = tex;
}