let camShader;
let cam;

function preload(){
  // load the shader
  camShader = loadShader('vid.vert', 'vid.frag');
}

function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
  noStroke();

  // initialize the webcam at the window size
  cam = createCapture(VIDEO);
  cam.size(windowWidth, windowHeight);
  cam.hide();

}

function draw() {  
  shader(camShader);

  camShader.setUniform('tex0', cam);
  camShader.setUniform('uMousePos', [map(mouseX, 0, width, 1.0, 0), map(mouseY, 0, height, 0, 1.0)]);
  camShader.setUniform('time', millis()/1000.0);

  // rect gives us some geometry on the screen
  rect(0,0,width, height);
}

function windowResized(){
  resizeCanvas(windowWidth, windowHeight);
}