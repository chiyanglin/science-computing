// The Nature of Code
// Daniel Shiffman
// Modified by Chiyang Lin

// Example 1-2: Bouncing Ball, with p5.Vector!

var number = 0;
var color = 51;
var colorstep = 0.01;
let mover;
var cnv;

function centerCanvas() {
  var x = (windowWidth - width) / 2;
  var y = (windowHeight - height) / 2;
  cnv.position(x, y);
}

function windowResized() {
  centerCanvas();
}

function setup() {
  //createCanvas(640, 360);
  cnv = createCanvas(640, 360);
  centerCanvas();
  background(255, 0, 200);
  mover = new Mover(width / 2, 30, 5);
  createP('Click mouse to apply wind force.');

  console.log("x = ",mover.position.x);
  console.log("y = ",mover.position.y);
  console.log("xspeed = ",mover.velocity.x);
  console.log("yspeed = ",mover.velocity.y);
}

function apply() {
  mover.position.x = parseFloat(document.getElementById("coord_x").value);
  mover.position.y = parseFloat(document.getElementById("coord_y").value);
  mover.velocity.x = parseFloat(document.getElementById("coord_speedx").value);
  mover.velocity.y = parseFloat(document.getElementById("coord_speedy").value);

  console.log("position.x = ",mover.position.x);
}

function draw() {

  background(51);

  let gravity = createVector(0, 1);
  //{!1} I should scale by mass to be more accurate, but this example only has one circle
  mover.applyForce(gravity);

  if (mouseIsPressed) {
    let wind = createVector(0.1, 0);
    mover.applyForce(wind);
  }

  if (mover.contactEdge()) {
    //{!5 .bold}
    let c = 0.1;
    let friction = mover.velocity.copy();
    friction.mult(-1);
    friction.setMag(c);

    //{!1 .bold} Apply the friction force vector to the object.
    mover.applyForce(friction);
  }

  mover.bounceEdges();
  mover.update();
  mover.display();

  document.getElementById("position_x").innerHTML = mover.position.x;
  document.getElementById("position_y").innerHTML = mover.position.y;
  document.getElementById("velocity_x").innerHTML = mover.velocity.x;
  document.getElementById("velocity_y").innerHTML = mover.velocity.y;
}
