/*
line movement function using mouse position referenced from:
http://www.itjavascript.com/how-do-i-change-the-color-only-once-upon-mousepressed-stop-it-from-cycling-ask-question/
*/

var u;
var l;
var a;
var mods = [];
var x;
var y;
var count;

function setup() {
  createCanvas(700, 600);
  u = 90;
  l = 20;
  var highCount = height / 80;
  var wideCount = width / 80;
  count = int(highCount * wideCount);

  var index = 0;
  for (var xc = 0; xc < wideCount; xc++) {
    for (var yc = 0; yc < highCount; yc++) {
      mods[index++] = new Module(int(xc) * u, int(yc) * u);
    }
  }
}

function draw() {

  if (mouseIsPressed) {
    background(0);
    stroke(255, 163, 163);
  } else {
    background(15, 150, 300);
    stroke(400);
  }

  strokeWeight(2);

  translate(30, 50);

  for (var i = 0; i <= 70; i++) {
    mods[i].update();
    mods[i].draw2();
  }

}

function Module(_x, _y) {
  this.x = _x;
  this.y = _y;
  this.a = 0;

}

Module.prototype.update = function() {
  if (mouseIsPressed) {
    this.a = -20 * (atan2(mouseY - this.y, mouseX - this.x));
  } else {
    this.a = atan2(mouseY - this.y, mouseX - this.x);
  }
}

Module.prototype.draw2 = function() {
  push();
  translate(this.x, this.y);
  rotate(this.a);
  line(-l, 0, l, 0);
  pop();
}