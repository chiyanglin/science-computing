// The Nature of Code
// Daniel Shiffman
// Modified by Chiyang Lin


// Pendulum

// A simple pendulum simulation
// Given a pendulum with an angle theta (0 being the pendulum at rest) and a radius r
// we can use sine to calculate the angular component of the gravitational force.

// Gravity Force = Mass * Gravitational Constant;
// Pendulum Force = Gravity Force * sine(theta)
// Angular Acceleration = Pendulum Force / Mass = gravitational acceleration * sine(theta);

// Note this is an ideal world scenario with no tension in the
// pendulum arm, a more realistic formula might be:
// Angular Acceleration = (g / R) * sine(theta)

// For a more substantial explanation, visit:
// http://www.myphysicslab.com/pendulum1.html
let pendulum;

//let mover;
//let attractor;
let input, button;
let xSlider, ySlider, xspeedSlider,yspeedSliders;
function setup() {
  createCanvas(800, 600);
  textSize(15);
  noStroke();
  // Make a new Pendulum with an origin position and armlength
  pendulum = new Pendulum(width / 2, 0, 175);
  // create sliders
  xSlider = createSlider(0, 100, 100);
  xSlider.position(20, 20);
  ySlider = createSlider(0, 10, 10);
  ySlider.position(20, 50);
  xspeedSlider = createSlider(50, 300, 2.5);
  xspeedSlider.position(20, 80);
  //yspeedSlider = createSlider(0, 10, 2);
  //yspeedSlider.position(20, 110);

  button = createButton('apply');
  button.position(20 , 140);
  button.mousePressed(apply);
  console.log("pendulum.x = ",pendulum.x);
  console.log("pendulum.y = ",pendulum.y);
  console.log("pendulum.r = ",pendulum.r);
}

function apply() {
  pendulum.x = xSlider.value();
  pendulum.y = ySlider.value();
  pendulum.r = xspeedSlider.value();
  //mover.velocity.y = yspeedSlider.value();
  console.log("pendulum.x = ",pendulum.x);
  console.log("pendulum.x = ",pendulum.y);
  console.log("pendulum.x = ",pendulum.r);
}

function draw() {
  background(51);
  text('x:', xSlider.x * 2 + xSlider.width, 35);
  text('y:', ySlider.x * 2 + ySlider.width, 65);
  text('r', xspeedSlider.x * 2 + xspeedSlider.width, 95);
  //text('y-speed', yspeedSlider.x * 2 + yspeedSlider.width, 125);

  pendulum.update();
  pendulum.drag(); // for user interaction
  pendulum.display();
}


function mousePressed() {
  pendulum.clicked(mouseX, mouseY);
}

function mouseReleased() {
  pendulum.stopDragging();
}
