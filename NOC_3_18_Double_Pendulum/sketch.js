let cx, cy;
let pathCanvas;
let doublePendulum;

function setup() {
  createCanvas(800, 800)
  pathCanvas = createGraphics(width, height)
  
  pathCanvas.colorMode(HSB, 1, 1, 1, 1)
  
  cx = width / 2
  cy = height * 0.4
  
  pathCanvas.translate(cx, cy)
  pathCanvas.background(0.5)
  
  doublePendulum = new DoublePendulum({g: 1})
}

function draw() {
  clear()
  imageMode(CORNER)
  image(pathCanvas, 0, 0, width, height)
  
  translate(cx, cy)
  doublePendulum.next()
  doublePendulum.draw()
  
  // pathCanvas.noStroke()
  
  pathCanvas.fill(1, 1, 1)
  pathCanvas.line(doublePendulum.x2, doublePendulum.y2, doublePendulum.x3, doublePendulum.y3)
}