class DoublePendulum {
  constructor({m1=15, m2=10, r1=75, r2=75, a1=PI/2, a2=PI/2, v1=0, v2=0, g=0.5, dampening=0.999}={}) {
    this.m1 = m1  // mass of bob 1
    this.m2 = m2  // mass of bob 2
    this.r1 = r1  // length of arm 1
    this.r2 = r2  // length of arm 2
    this.a1 = a1  // angle of arm 1
    this.a2 = a2  // angle of arm 2
    this.v1 = v1  // angular velocity of arm 1
    this.v2 = v2  // angular velocity of arm 2
    this.acc1 = 0 // angular acceleration of arm 1
    this.acc2 = 0 // angular acceleration of arm 2
    this.g = g    // gravitational constant (real ~= 9.8/60/60 in 60fps)
    this.dampening = dampening // angular velocity dampening factor
    this.calculatePositions()  // calculate initial bob positions
  }
  
  draw() {
    line(0, 0, this.x1, this.y1)
    line(this.x1, this.y1, this.x2, this.y2)
    push()
    noStroke()
    circle(this.x1, this.y1, this.m1)
    circle(this.x2, this.y2, this.m2)
    pop()
  }
  
  drawTriangle() {
    triangle(0, 0, this.x1, this.y1, this.x2, this.y2)
  }
  
  next() {
    this.calculateAccelerations()
    this.calculateVelocities()
    this.calculatePositions()
  }
  
  calculatePositions() {
    this.x3 = this.x2
    this.y3 = this.y2
    this.a1 += this.v1
    this.a2 += this.v2
    this.x1 = this.r1 * sin(this.a1)
    this.y1 = this.r1 * cos(this.a1)    
    this.x2 = this.x1 + this.r2 * sin(this.a2)
    this.y2 = this.y1 + this.r2 * cos(this.a2)
  }
  
  calculateVelocities() {
    this.v1 += this.acc1
    this.v2 += this.acc2
    this.v1 *= this.dampening
    this.v2 *= this.dampening
  }
  
  calculateAccelerations() {
    let {m1, m2, r1, r2, a1, a2, v1, v2, g} = this
    let denominator = r1 * (2 * m1 + m2 - m2 * cos(2 * a1 - 2 * a2)) 
    this.acc1 = (
      -g * (2 * m1 + m2) * sin(a1) +
      -m2 * g * sin(a1 - 2 * a2) +
      -2 * sin(a1 - a2) * m2 *
      (v2 * v2 * r2 + v1 * v1 * r1 * cos(a1 - a2))
    ) / denominator
    this.acc2 = (
      2 * sin(a1 - a2) * (
        v1 * v1 * r1 * (m1 + m2) +
        g * (m1 + m2) * cos(a1) +
        v2 * v2 * r2 * m2 * cos(a1 - a2)
      )
    ) / denominator
  }
  
  
  
}
