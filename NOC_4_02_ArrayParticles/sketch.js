// The Nature of Code
// Daniel Shiffman
// Modified by Chiyang Lin


let particles = [];

let input, button;
let xSlider, ySlider, xspeedSlider,yspeedSliders;
function setup() {
  createCanvas(800, 600);
  textSize(15);
  noStroke();

  // create sliders
  xSlider = createSlider(0, 800, 100);
  xSlider.position(20, 20);
  ySlider = createSlider(0, 600, 100);
  ySlider.position(20, 50);
  xspeedSlider = createSlider(0, 10, 2.5);
  xspeedSlider.position(20, 80);
  yspeedSlider = createSlider(0, 10, 2);
  yspeedSlider.position(20, 110);

  button = createButton('apply');
  button.position(20 , 140);
  button.mousePressed(apply);
  //console.log("x = ",mover.position.x);
}

function apply() {
  //mover.position.x = xSlider.value();
  //mover.position.y = ySlider.value();
  //mover.velocity.x = xspeedSlider.value();
  //mover.velocity.y = yspeedSlider.value();
}

function draw() {
  background(51);
  text('x:', xSlider.x * 2 + xSlider.width, 35);
  text('y:', ySlider.x * 2 + ySlider.width, 65);
  text('x-speed', xspeedSlider.x * 2 + xspeedSlider.width, 95);
  text('y-speed', yspeedSlider.x * 2 + yspeedSlider.width, 125);


  particles.push(new Particle(createVector(width / 2, 50)));

  // Looping through backwards to delete
  for (var i = particles.length - 1; i >= 0; i--) {
    var p = particles[i];
    p.run();
    if (p.isDead()) {
      //remove the particle
      particles.splice(i, 1);
    }
  }
  console.log("particles.length =",particles.length );
}
