// The Nature of Code
// Daniel Shiffman
// Modified by Chiyang Lin

// Example 1-2: Bouncing Ball, with p5.Vector!

var number = 0;
var color = 51;
var colorstep = 0.01;
let mover;
let attractor;
var cnv;

function centerCanvas() {
  var x = (windowWidth - width) / 2;
  var y = (windowHeight - height) / 2;
  cnv.position(x, y);
}

function windowResized() {
  centerCanvas();
}

function setup() {
  //createCanvas(640, 360);
  cnv = createCanvas(640, 360);
  centerCanvas();
  background(255, 0, 200);
  mover = new Mover(300, 100, 2);
  attractor = new Attractor();

  console.log("x = ",mover.position.x);
  console.log("y = ",mover.position.y);
  console.log("xspeed = ",mover.velocity.x);
  console.log("yspeed = ",mover.velocity.y);
}

function apply() {
  mover.position.x = parseFloat(document.getElementById("coord_x").value);
  mover.position.y = parseFloat(document.getElementById("coord_y").value);
  mover.velocity.x = parseFloat(document.getElementById("coord_speedx").value);
  mover.velocity.y = parseFloat(document.getElementById("coord_speedy").value);

  console.log("position.x = ",mover.position.x);
}

function draw() {

  background(51);
  let force = attractor.attract(mover);
  mover.applyForce(force);
  mover.update();

  attractor.display();
  mover.display();

  document.getElementById("position_x").innerHTML = mover.position.x;
  document.getElementById("position_y").innerHTML = mover.position.y;
  document.getElementById("velocity_x").innerHTML = mover.velocity.x;
  document.getElementById("velocity_y").innerHTML = mover.velocity.y;
}

function mouseMoved() {
  attractor.handleHover(mouseX, mouseY);
}

function mousePressed() {
  attractor.handlePress(mouseX, mouseY);
}

function mouseDragged() {
  attractor.handleHover(mouseX, mouseY);
  attractor.handleDrag(mouseX, mouseY);
}

function mouseReleased() {
  attractor.stopDragging();
}
