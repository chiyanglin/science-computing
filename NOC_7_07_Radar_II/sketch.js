const scale = 20
const width = 400
const step = width / scale / 2
function setup() {
  background(255)
  createCanvas(width, width)
}

function draw() {
  background("rgba(255,255,255,0.05)");
  translate(width/2, width/2)
  for(let i = 0; i < scale; i++)
  {
    fill(0)
    let xm = cos(frameCount/PI/scale)
    let ym = sin(frameCount/PI/scale)
    ellipse(step * i * xm, step * i * ym, step,step)
  }
}